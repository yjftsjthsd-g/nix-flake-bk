# nix-flake-bk

A nix flake to build bk, the terminal epub reader.


## Use

Prerequisite: You'll need a flake-enabled nix system; see https://nixos.wiki/wiki/Flakes for details.

To run:
```
nix run gitlab:yjftsjthsd-g/nix-flake-bk -- book.epub
```


## License

This repo - a bunch of build scripts, basically - is released under the MIT
license. The bk program and its libraries are under their own
copyright/licenses; do your own due diligence.

