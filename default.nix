{ pkgs ? import <nixpkgs> {} }:

with pkgs;

rustPlatform.buildRustPackage rec {
  pname = "bk";
  version = "0.6.0";
  src = fetchurl {
    url = "https://github.com/aeosynth/bk/archive/refs/tags/v${version}.tar.gz";
    hash = {
      "0.6.0" = "sha256-xyDI6B6GcJ+FQ8oal6PDCzuzPVVpKlNs7+0K0uPfq80=";
    }.${version} or "sha256-0000000000000000000000000000000000000000000000000000";
  };
  cargoHash = {
    "0.6.0" = "sha256-pE5loMwNMdHL3GODiw3kVVHj374hf3+vIDEYTqvx5WI=";
  }.${version} or "sha256-0000000000000000000000000000000000000000000000000000";
}
